import subprocess
from time import sleep
from sys import argv

proc1 = subprocess.Popen(["python3", "server.py"] + argv[1:])
sleep(2)
proc2 = subprocess.Popen(["python3", "test.py"] + argv[1:])
proc1.wait()
proc2.wait()
proc1.kill()
proc2.kill()