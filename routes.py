import requests
from time import sleep
import db

myapikey = 'f2a838e95d14f0b506c8391ae8e287ff39461c599a47a2312d75cd580ad5020e'
scan_url = "https://www.virustotal.com/vtapi/v2/url/scan"
report_url = "https://www.virustotal.com/vtapi/v2/url/report"


def check_url(payload):
    print("routes: in check_url")
    try:
        url = payload.split('=')[1]
    except IndexError as e:
        # print(e)
        print("routes: Problem")
    
    print(url)
    
    # Checking if url exists in DB
    db_check = db.check_if_exists_in_db(url)
    if not db_check == None:
        return True if db_check == 1 else False 
    
	# Sending HTML POST request to virustotal with the URL to inspect
    params = {'apikey': myapikey, 'url':url}
    while True:
        print("Sending HTML POST request to VS: {0}".format(url))
        scan_response = requests.post(scan_url, data=params)
        if scan_response.status_code == 204:
            sleep(15)
        elif scan_response.status_code == 200:
            break
        
    print(scan_response.json()['verbose_msg'])
    if "later" in scan_response.json()['verbose_msg']:
        scan_id = scan_response.json()['scan_id']
        report_params = {'apikey': myapikey, 'resource': scan_id}
        while True:
            print("Sending HTML GET request to retrieve report on: {0}".format(url))
            report_response = requests.get(report_url, params=report_params)
            if report_response.status_code == 204:
                sleep(15)
            elif report_response.status_code == 200:
                break
        
        print(report_response.json()['verbose_msg'])
        res = report_response.json()
        
    else:
        print(scan_response.json()['verbose_msg'])
        res = scan_response.json()
    
    db.insert_url(url, 1 if res['positives'] == 0 else 0, res['scan_id'])
    return res['positives'] == 0