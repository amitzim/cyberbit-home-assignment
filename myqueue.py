import queue


# replace with singleton pattern
q = None

def init_queue():
    global q
    q = queue.Queue()
    
# class Singleton_queue():
    # pass
    
def get_queue():
    global q
    if not q:
        init_queue()
    return q