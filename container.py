from threading import Condition
"""
Class made for containing data on Tasks
    - Payload is the argument for the task
    - Result is where the result of task_name(payload) lies after consumer finished
    - CV is a Condition Object where the producer waits on it until consumer call notify()
"""

class Container():

    def __init__(self, payload, task_name):
        self.payload = payload
        self.task_name = task_name
        self.result = False
        self.cv = Condition()
        
    
    def wait(self):
        with self.cv:
            self.cv.wait()
        

    def notify(self):
        with self.cv:
            self.cv.notify()