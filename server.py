from http.server import SimpleHTTPRequestHandler
import socketserver
import routes
from consumer import consume
from container import Container
from myqueue import get_queue
from sys import argv
import logging
import threading
from db import create_db
PORT = int(argv[1])

class myHTTPHandler(SimpleHTTPRequestHandler):
    def __init__(self, request, client_address, server):
        SimpleHTTPRequestHandler.__init__(self, request, client_address, server)

    def do_POST(self):
        logging.debug(self.path)
        logging.debug("server: in do_POST")
        
        path = self.path.lstrip('/')
        payload = self.rfile.read(int(self.headers['Content-Length'])).decode('utf-8')

        c = Container(payload, path)
        logging.debug("server: inserting task to queue")
        get_queue().put(c)
        logging.debug("waiting for consumer")
        c.wait()
        logging.debug("notified by consumer")
        logging.debug("path: " + payload + " result is:" + str(c.result))
        self.send_response(200)
        self.send_header("Content-type",  "text/html; charset=utf-8")
        #self.send_header("Access-Control-Allow-Origin", "*")
        self.end_headers()
        self.wfile.write(bytes("Is Safe? ".encode('utf-8') + str(c.result).encode('utf-8')))
        # self.wfile.close()
        
        
       
def Main():
    logging.basicConfig(filename='server.log',level=logging.DEBUG)
    handler = myHTTPHandler
    httpd = socketserver.ThreadingTCPServer(("", PORT), handler)
    get_queue()
    create_db()
    consumer = threading.Thread(target = consume)
    try:
        consumer.start()
        logging.debug("server: Starting server")
        httpd.serve_forever()
    except KeyboardInterrupt:
        logging.debug("server: Shutting down server")
        # Consumer thread still working...
        httpd.shutdown()
        httpd.server_close()

    
if __name__ == '__main__':
    Main()