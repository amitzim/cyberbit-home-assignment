import routes
from myqueue import get_queue
import logging

def consume():
    logging.basicConfig(filename='server.log',level=logging.DEBUG)
    logging.debug("consumer: consumer thread started")
    while True:
        if not get_queue().empty():
            logging.debug("consumer: consumer spins")
            c = get_queue().get()
            try:
                logging.debug(c.payload)
                res = getattr(routes, str(c.task_name))(c.payload)
                c.result = res
            except AttributeError as e:
                logging.error(e)
                c.result = False
            
            logging.debug("consumer: result of {0}: {1}".format(str(c.payload), str(c.result)))
            c.notify()
