import requests
from sys import argv
from time import sleep

url_to_list = "https://gist.githubusercontent.com/demersdesigns/4442cd84c1cc6c5ccda9b19eac1ba52b/raw/cf06109a805b661dd12133f9aa4473435e478569/craft-popular-urls"
r = requests.get(url_to_list)
lst = r.content.decode('utf-8').split('\n')

print("test.py: sending requests")
for i, u in enumerate(lst, start = 1):
    try:
        u = u.lstrip("http://").rstrip('/')
        print("test.py: ", u)
        response = requests.post("http://localhost:{0}/check_url".format(argv[1]), data = {"url" : u})
        sleep(1)
        print("test.py response:", response.content)
    except Exception as e:
        print(e)
    print(i)