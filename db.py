import sqlite3
import os

db_filename = "URLs data.db"

def create_db():
    db_existed = os.path.isfile(db_filename)
    with sqlite3.connect(db_filename) as dbcon:
        cursor = dbcon.cursor()
        if not db_existed:
            print("db: Creating Database")
            cursor.execute("""CREATE TABLE URLs (
                            Number INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                            URL TEXT NOT NULL,
                            Safe INTEGER NOT NULL,
                            Scan_id TEXT NOT NULL )""")
            
    dbcon.commit()
    dbcon.close()
    

def insert_url(url, safe, scan_id):
    print("db: Inserting {0} to DB".format(url))
    with sqlite3.connect(db_filename) as dbcon:
        cursor = dbcon.cursor()
        cursor.execute("""INSERT INTO URLs (URL, Safe, Scan_id) VALUES (?, ?, ?)""", [url, safe, scan_id])
    
    dbcon.commit()
    dbcon.close()
    
def check_if_exists_in_db(url):
    print("db: Checking if {0} in DB".format(url))
    with sqlite3.connect(db_filename) as dbcon:
        cursor = dbcon.cursor()
        cursor.execute("""SELECT Safe FROM URLs WHERE URL = ?""", [url])
        output = cursor.fetchone()
    
    dbcon.commit()
    dbcon.close()
    return None if output == None else output[0]